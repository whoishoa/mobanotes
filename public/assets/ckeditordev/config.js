﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// %REMOVE_START%
	config.contentsCss = ['assets/ckeditordev/contents.css', 'assets/20-50-50-fonts.css'];	
	config.shiftEnterMode = CKEDITOR.ENTER_P;
	config.autoGrow_maxHeight = 250;
	config.autoGrow_minHeight = 51;
	config.height = 100;
	// config.uiColor = '#FFFFFF';
	config.plugins =
		'autogrow,' +
		'basicstyles,' +
		'blockquote,' +
		'clipboard,' +
		'colorbutton,' +
		'colordialog,' +
		'contextmenu,' +
		'div,' +
		'enterkey,' +
		'entities,' +
		'filebrowser,' +
		'floatingspace,' +
		'font,' +
	//	'format,' +
		'image,' +
		'iframe,' +
		'indentlist,' +
		'justify,' +
		'link,' +
		'list,' +
		'liststyle,' +
	//	'maximize,' +
		'pastefromword,' +
		'pastetext,' +
		'preview,' +
		'print,' +
		'showborders,' +
		'specialchar,' +
//		'stylescombo,' +
		'tab,' +
		'table,' +
		'tabletools,' +
		'toolbar,' +
		'undo,' +
		'wysiwygarea';
	// %REMOVE_END%
};

// %LEAVE_UNMINIFIED% %REMOVE_LINE%
