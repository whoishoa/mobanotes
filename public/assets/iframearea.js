
$(document).ready(function() {
  // called when the wsiwyg iframe is finished loading
  $("body").attr("id", "mainBody");
  var b = document.getElementById("mainBody");
  b.addEventListener("dragover", filesDraggedOver, false);
  b.addEventListener("dragleave", filesDraggedLeave, false);
  b.addEventListener("drop", filesSelectHandler, false);
  
});

var dropboxvisible = false;

function filesDraggedOver(e) {
  // e.preventDefault();
  if (!dropboxvisible) {
    dropboxvisible = true;
    $("body").prepend("<div id='dropbox' class='dropbox'><div>drop files here</div></div>");
  }
}



function filesDraggedLeave(e) {
  $(".dropbox").remove();
  dropboxvisible = false;
}

function filesSelectHandler(e) {
  e.stopPropagation();
  e.preventDefault();
  
}
