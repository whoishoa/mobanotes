class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_filter :startPage
  after_filter :cors

  def cors
    headers['X-Frame-Options'] = "ALLOWALL"
    headers['Access-Control-Allow-Origin'] = '*' 
    headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS' 
    headers['Access-Control-Request-Method'] = '*' 
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end

  
  # This method is executed when each page is loaded
  def startPage
    setSessionUser(@remote_user)
    
    incrementSessionUserPageViews
  end
  
  def setSessionUser(user)
    if IP_IGNORE.include? request.remote_ip
      session.clear
    elsif ! (session.has_key? :user)
      u = User.create(:ip => request.remote_ip, :color => getRandomStartColor)
      session[:user] = u
    else
      session[:user] = User.find(session[:user].id) # When session[:user] data is changed via websockets, session[:user] doesn't get updated when you refresh unless you add this line
    end
  end
  
  # Increments the number of page views for the current user
  def incrementSessionUserPageViews
    User.increment_counter(:pageviews, session[:user].id)
  end
  
  # This method returns a distinct hex color string 
  # without the "#" at the beginning.
  # This color is guaranteed to be somewhat dark.
  # This method randomizes two RGB values from 0 to 160
  # and the third RGB value from 0 to 250
  def getRandomStartColor
    main = (60 + Random.rand(17) * 10).to_s(16)
    if main.length == 1
      main = "0" + main
    end
    secondary = (Random.rand(18) * 10).to_s(16)
    if secondary.length == 1
      secondary = "0" + secondary
    end
    third = "20"
    mainindi = Random.rand(3)
    secindi = (mainindi + Random.rand(2) + 1) % 3
    ret = main
    if mainindi == 0 
      if secindi == 1
        ret += secondary + third
      else
        ret += third + secondary
      end
    elsif mainindi == 1
      if secindi == 2
        ret = third + main + secondary 
      else
        ret = secondary + main + third
      end
    else
      if secindi == 0
        ret = secondary + third + main 
      else
        ret = third + secondary + main
      end
    end
    return ret
  end
  
  # This method returns a distinct hex color string 
  # without the "#" at the beginning.
  # This method randomizes two RGB values from 0 to 160
  # and the third RGB value from 0 to 250
  def getRandomColor    
    main = (90 + Random.rand(17) * 10).to_s(16)
    if main.length == 1
      main = "0" + main
    end
    secondary = (Random.rand(26) * 10).to_s(16)
    if secondary.length == 1
      secondary = "0" + secondary
    end
    third = (Random.rand(17) * 10).to_s(16)
    if third.length == 1
      third = "0" + third
    end
    mainindi = Random.rand(3)
    secindi = (mainindi + Random.rand(2) + 1) % 3
    ret = main
    if mainindi == 0 
      if secindi == 1
        ret += secondary + third
      else
        ret += third + secondary
      end
    elsif mainindi == 1
      if secindi == 2
        ret = third + main + secondary 
      else
        ret = secondary + main + third
      end
    else
      if secindi == 0
        ret = secondary + third + main 
      else
        ret = third + secondary + main
      end
    end
    return ret
  end
  
end
