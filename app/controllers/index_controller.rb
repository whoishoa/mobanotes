require 'base64'
require 'openssl'
require 'digest/sha1'

class IndexController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # The main page for the application.
  # URL: GET /
  def home

  end

  def chat

  end

  def testlab
    policy_document = 
"{\"expiration\": \"2837-08-01T00:08:50.715Z\",
  \"conditions\": [ 
    {\"bucket\": \"mobahive\"}, 
    [\"starts-with\", \"$key\", \"uploads/\"],
    {\"acl\": \"public-read\"},
    {\"success_action_redirect\": \"http://localhost:3000/success\"},
    [\"starts-with\", \"$Content-Type\", \"\"],
    [\"content-length-range\", 0, 1048576]
  ]
}"
    @policy = Base64.encode64(policy_document).gsub("\n","")
    aws_secret_key = "ChA3HHBPkAONchdV31uun/0VIQ2J/btBxWRZFGi1"
    @signature = Base64.encode64(
        OpenSSL::HMAC.digest(
            OpenSSL::Digest::Digest.new('sha1'), 
            aws_secret_key, @policy)
        ).gsub("\n","")
  end

  def oembed
    @message = params
    @url = params[:url]
  end

  # This is the main page for the main board page of the website.
  # URL: GET /:title
  def publicboard
    @board = Board.where("title = ?", params["title"].downcase )
    if @board.count == 0
      @board = Board.create(:title => params["title"].downcase )
    else
      @board = @board.first
      Board.increment_counter(:visits, @board.id)
    end
    @comments = @board.comments.where("ctype > 0").last(LOAD_AMOUNT)
    puts @comments
    @LOAD_AMOUNT = LOAD_AMOUNT
  end

end
