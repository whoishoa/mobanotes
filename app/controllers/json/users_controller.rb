class Json::UsersController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
 
  
  # Randomize a new color for the current user.
  # URL: POST /json/users
  def create
    @response = {}
    if params[:property] == "color" 
      @response["color"] = session[:user].color = getRandomColor
    elsif params[:property] == "enter"
      if params[:newvalue] == "1"
        session[:user].enterpress = true
      else
        session[:user].enterpress = false
      end
      @response["enter"] = session[:user].enterpress
    else
      @response["error"] = "No valid property specified: " + params[:property]
    end
    session[:user].save()
    
    render :json => @response
  end
  
end
