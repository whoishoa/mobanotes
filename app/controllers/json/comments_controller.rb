class Json::CommentsController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # Gets all of the comments 
  # URL: GET /comments
  def index
    render :json => session[:user]
  end

  # Gets all of the comments for a particular board
  # URL: GET /comments/:id
  #
  # Parameters:
  #   :id - The id of the board to get the comments of.
  #   position - The position of the id last displayed by the current board.
  def show
    @response = {}
    begin
      b = Board.find(params[:id])
      @response["result"] = "success"
      if params[:reverse] == "yes"
        r = b.comments.where("id < ?", params[:position])
        @response["comments"] = r.last(LOAD_AMOUNT)
        if r.count <= LOAD_AMOUNT
          @response["end"] = true;
        else
          @response["end"] = false;
        end
      else
        @response["comments"] = b.comments.where("id > ?", params[:position])
      end
      #
      #checks = 0
      #while @response["comments"].count == 0 and checks < 100
      #  sleep(1.0/2.0)
      #  @response["comments"] = b.comments.where("id > ?", params["position"])
      #  checks += 1;
      #end
      
    rescue ActiveRecord::RecordNotFound # no board with that id
      @response["result"] = "failed"
    end
    
    render :json => @response
  end
  
  # Creates a new comment
  # URL: POST /comments
  #
  # Parameters:
  #   board - The id of the board to add the comment to.
  #   message - The message of the comment
  #   position - The position of the id last displayed by the current board
  def create
    @response = {}
    comm = format(params["message"])
    if comm == ""
      @response["result"] = "failed"
      @response["reason"] = "For aesthetic purposes, you cannot add comments with just whitespace."
    else
      begin
        b = Board.find(params["board"])
        @response["added"] = b.comments.create( 
            :comment => comm, 
            :user => session[:user],
            :color => session[:user].color )
        @response["result"] = "success"
        @response["comments"] = b.comments.where("id > ?", params["position"])
      rescue ActiveRecord::RecordNotFound #no board with that id
        @response["result"] = "failed"
      end
    end
    
    render :json => @response
  end
  
  def format(comment)
    comment.strip!
    comment.gsub!("\r\n", "[br]");
    comment.gsub!("\n", "[br]");
    comment.gsub!("\r", "[br]");
    return comment
  end
  
end
