
require 'base64'
require 'open-uri'
class Commands::CommandsController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def clearsession
    session.clear
    render :json => {}
  end

  def encode
    encoded = Base64.encode64(open(params[:url], &:read) )
    render :json => {
      :encoded => encoded
    }
    
  end
  
end
