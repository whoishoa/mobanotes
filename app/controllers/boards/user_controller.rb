class Boards::UserController < WebsocketRails::BaseController

  def initialize_session
    puts "Session Initialized\n"
  end
  
  def client_connected
    puts "client #{client_id} connected"
    system_msg :new_message, "client #{client_id} connected"
  end
  
  def client_disconnected
    puts "client #{client_id} disconnected"
    system_msg :new_message, "client #{client_id} disconnected"
  end
  
  def change_color
    @response = {}
    @response["color"] = session[:user].color = getRandomColor
    session[:user].save()
    trigger_success @response
  end
  
  def toggle_enter
    @response = {}
    if message[:newvalue] == 1
      session[:user].enterpress = true
    else
      session[:user].enterpress = false
    end
    @response["enter"] = session[:user].enterpress
    session[:user].save()
    trigger_success @response
  end

  # def system_msg(ev, msg)
    # broadcast_message ev, { 
      # user_name: 'system', 
      # received: Time.now.to_s(:short), 
      # msg_body: msg
    # }
  # end
#   
  # def user_msg(ev, msg)
    # broadcast_message ev, { 
      # user_name:  connection_store[:user][:user_name], 
      # received:   Time.now.to_s(:short), 
      # msg_body:   ERB::Util.html_escape(msg) 
      # }
  # end
#   
  # def new_message
    # user_msg :new_message, message[:msg_body].dup
  # end
#   
  # def new_user
    # connection_store[:user] = { user_name: sanitize(message[:user_name]) }
    # broadcast_user_list
  # end
#   
  # def change_username
    # connection_store[:user][:user_name] = sanitize(message[:user_name])
    # broadcast_user_list
  # end
#   
  # def delete_user
    # connection_store[:user] = nil
    # system_msg "client #{client_id} disconnected"
    # broadcast_user_list
  # end
#   
  # def broadcast_user_list
    # users = connection_store.collect_all(:user)
    # broadcast_message :user_list, users
  # end
  
end