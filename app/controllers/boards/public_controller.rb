class Boards::PublicController < WebsocketRails::BaseController
  
  def add_message
    @response = {}
    comm = format(message[:message])
    if comm == ""
      @response["result"] = "failed"
      @response["reason"] = "For aesthetic purposes, you cannot add comments with just whitespace."
    else
      begin
        b = Board.find(message[:board])
        @response["added"] = b.comments.create( 
            :comment => comm, 
            :user => session[:user],
            :color => session[:user].color )
        @response["result"] = "success"
        @response["comments"] = b.comments.where("ctype > 0 AND id > ?", message[:position])
        WebsocketRails[b.title].trigger(:new_comment, @response)
      rescue ActiveRecord::RecordNotFound #no board with that id
        puts "failed"
        @response["result"] = "failed"
        
        trigger_failure @response
      end
    end
  end
  
  def delete_message
    @response = {}
    
    begin
      b = Board.find(message[:board])
      toRemove = b.comments.find(message[:commentid])
      toRemove.ctype = -1
      toRemove.save()
      @response["result"] = "success"
      @response["deletedid"] = message[:commentid]
      WebsocketRails[b.title].trigger(:deleted_comment, @response)
    rescue ActiveRecord::RecordNotFound #no board with that id
      puts "failed"
      @response["result"] = "failed"
      
      trigger_failure @response
    end
  end
  
  def edit_message
    @response = {}
    comm = format(message[:message])
    if comm == ""
      @response["result"] = "failed"
      @response["reason"] = "For aesthetic purposes, you cannot add comments with just whitespace."
    else
      begin
        b = Board.find(message[:board])
        @response["edited"] = Comment.update(message[:position], :comment => message[:message])
        @response["result"] = "success"
        WebsocketRails[b.title].trigger(:edited_comment, @response)
      rescue ActiveRecord::RecordNotFound #no board with that id
        puts "failed"
        @response["result"] = "failed"
        
        trigger_failure @response
      end
    end
  end
  
  def load_posts
    @response = {}
    
    begin
      b = Board.find(message[:board])
      @response["result"] = "success"
      if message[:reverse] == "yes"
        r = b.comments.where("ctype > 0 AND id < ?", message[:position])
        @response["comments"] = r.last(LOAD_AMOUNT)
        if r.count <= LOAD_AMOUNT
          @response["end"] = true;
        else
          @response["end"] = false;
        end
      else
        @response["comments"] = b.comments.where("ctype > 0 AND id > ?", message[:position])
      end
      
    rescue ActiveRecord::RecordNotFound # no board with that id
      @response["result"] = "failed"
    end
    trigger_success @response
  end
  
  def interval_posts
    @response = {}
    begin
      b = Board.find(message[:board])
      @response["result"] = "success"
      
      if message[:method] == 1 # url changed
        u = b.comments.where("ctype > 0 AND id < ? AND id > ?", message[:tofind], message[:uplimit]).last(UP_LOAD_AMOUNT) 
        setUpMore(@response, u, b, message)
        d = (message[:downlimit] != "max") ? 
            b.comments.where("ctype > 0 AND id >= ? AND id < ?", message[:tofind], message[:downlimit]).first(DOWN_LOAD_AMOUNT) :
            b.comments.where("ctype > 0 AND id >= ?", message[:tofind]).first(DOWN_LOAD_AMOUNT)
        @response["comments"] = u + d
        setDownMore(@response, d, b, message)
      elsif message[:method] == 2 # reverse
        u = b.comments.where("ctype > 0 AND id <= ? AND id >= ?", message[:downlimit], message[:uplimit]).last(LOAD_AMOUNT) 
        @response["comments"] = u
        setUpMore(@response, u, b, message)
        @response["downend"] = true
      elsif message[:method] == 3 # no reverse
        d = b.comments.where("ctype > 0 AND id >= ? AND id <= ?", message[:tofind], message[:downlimit]).first(LOAD_AMOUNT)
        @response["comments"] = d
        setDownMore(@response, d, b, message)
        @response["upend"] = true
      end
      @response["tofind"] = message[:tofind];
      @response["method"] = message[:method];
    rescue ActiveRecord::RecordNotFound # no board with that id
      @response["result"] = "failed"
    end
    trigger_success @response
  end
  
  # Helper methods
  def setDownMore(response, d, b, message)
    if d.count > 0 and 
        (x = (message[:downlimit] != "max") ? 
            b.comments.where("ctype > 0 AND id > ? AND id < ?", d.last.id, message[:downlimit]).count :
            b.comments.where("ctype > 0 AND id >= ?", message[:tofind]).count
        ) > 0
      response["downend"] = false;
      response["downmore"] = x
    else
      response["downend"] = true;
    end
  end
  
  def setUpMore(response, u, b, message)
    if u.count > 0 and (x = b.comments.where("ctype > 0 AND id < ? AND id > ?", u.first.id, message[:uplimit]).count) > 0
      response["upend"] = false;
      response["upmore"] = x
    else
      response["upend"] = true;
    end 
  end
  
  def format(comment)
    comment.strip!
    comment.gsub!("\r\n", "[br]");
    comment.gsub!("\n", "[br]");
    comment.gsub!("\r", "[br]");
    return comment
  end
  
end
