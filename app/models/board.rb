class Board < ActiveRecord::Base
  has_many :comments, :dependent => :destroy
  
  validates_uniqueness_of :title, :case_sensitive => false
end
