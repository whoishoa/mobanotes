class Comment < ActiveRecord::Base
  belongs_to :board, :class_name => 'Board'
  belongs_to :user, :class_name => 'User'
end
