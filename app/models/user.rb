class User < ActiveRecord::Base
  # You might want to remove the dependent destroy but then you will have to deal
  # with more edge cases such as if a comment doesn't have a user.
  has_many :comments, :dependent => :destroy 
end
