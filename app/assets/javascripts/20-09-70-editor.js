
/* This object handles everything that has to do with text editor */
function editorHandler(id, afterLoad) {
  
  
  
  var shiftOn = false;
  var geturl = new RegExp( // Improve this when you have time
          "(^|[ \t\r\n;>.,])((ftp|http|https|gopher|mailto|news|nntp|telnet|wais|file|prospero|aim|webcal):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))"
         ,"g"
  );
  var youtubeUrl = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  var HTTPS_IMG_PROXY = "https://images.weserv.nl/?url=";
  var justchanged = true;
  var thumbnails = []; //keeps track of thumbnails that need to be switched with videos
  
  var self = this;

  
  this.init = function(id) {
    this.editorId = "#cke_" + id;
    this.editor = CKEDITOR.replace( id, {
        height:52,
       // uiColor: '#FFFFFF',
        font_defaultLabel: 'Open Sans'
    } );
    console.log(this.editor);
      
    this.editor.on("instanceReady", editorLoaded);
    this.editor.on("change", editorChanged);
    this.editor.on("key", editorKeyDown);
  
    this.editor.on("focus", function() {
      // Neccessary because onClickOutside doesn't occur when you click on an iframe
      APP.UH.handleHideOutsides(true);
    });
    
    this.editor.on("resize", positionToolbar);
  
    // Submits the search bar when enter is pressed.
    $('#comment').keypress(function(e) {
      if (e.which == 13) {
        if ($("#enterCheckbox").is(":checked") || APP.CH.shiftOn) {
          setTimeout(APP.CH.addComment, 100);
          APP.shiftOn = false;
        }
      }
    });
  };

  //switches out the thumbnail for the embedded Youtube Video
  this.addVideo = function(message) {
    var newMessage = message;
    if (thumbnails.length > 0) {
      for (var i = 0; i < thumbnails.length; i++) {
        newMessage = newMessage.replace(thumbnails[i][0].slice(0, -1)+ " />", "<iframe width='560' height='315' src='https://www.youtube.com/embed/" + thumbnails[i][1] + "' frameborder='0' allowfullscreen></iframe>");
      }
    }
    return newMessage;
  };

  this.getComment = function() {
    return this.editor.getData().replace(/"/g, "'");
  };

  this.getFilteredComment = function() {
    var c = this.getComment();
    var ending = "<p>&nbsp;</p>";
    while (c.indexOf(ending) == 0) {
      c = c.slice(ending.length);
    }
    while (c.indexOf(ending) == c.length - ending.length) {
      c = c.slice(0, -1 * ending.length);
    }
    return c;
  };

  this.clearComment = function() {
    this.editor.setData("");
  };

  this.isWhitespace = function() {
    var check = this.getComment();
    check = check.replace(/<p>/g, "");
    check = check.replace(/<\/p>/g, "");
    check = check.replace(/&nbsp;/g, "");
    if (/^\s*$/.test( check )) {
      return true;
    }
    return false;
  };

  var editorLoaded = function() {
    getToolbarElement(".cke_button__preview").remove();
    getToolbarElement(".cke_button__cut").remove();
    // $("#cke_11, #cke_14").remove();

    moveAfter(getToolbarElement(".cke_button__image"), getToolbarElement(".cke_button__bgcolor")); // B I U S x x2 bar 
    addSeparator( getToolbarElement(".cke_button__bgcolor") );
    if (self.editorId == APP.EH.editorId) {
      $(self.editorId + " .cke_top").addClass("commentToolbar");
      $(self.editorId + " .cke_top").hide();
      
      self.editor.on("focus", focusEditor);
      self.editor.on("blur", blurEditor);
    }
    
    if (afterLoad) {
      afterLoad(self);
    }
    
    $("footer").show();
    
    APP.CH.scrollIntoView(APP.CH.startPage);
    
    //self.FM = new CKEDITOR.focusManager( self.editor );
    /*
    addSeparator("#cke_39"); // last align
    addSeparator("#cke_9"); // font
    addSeparator("#cke_10"); // size
    addSeparator("#cke_52"); // highlight color
    */
    // show the div holding the contents
  };
  
  var focusEditor = function() {
    positionToolbar();
    $(self.editorId + " .commentToolbar").fadeIn(300);
    
  };
  
  var blurEditor = function() {
    $(self.editorId + " .commentToolbar").fadeOut(300);
  };
  
  var positionToolbar = function() {
    var tb = $(self.editorId + " .commentToolbar");
    if (tb.length != 0) {
      var ed = $(self.editorId);
      tb.css("width", ed.width() - 4);
      tb.css("left", ed.offset().left);
      tb.css("top", ed.offset().top - tb.height() - 3);
    }
  };
  
  this.resized = function() {
    positionToolbar();
  };

  var editorChanged = function(evt) {
    if (justchanged) {
      justchanged = false;
      return;
    }

    var urls = getURLs(self.getComment());

    if (urls && urls.length > 0) {
      for (var i = 0; i < urls.length; i++) {
        // console.log(urls[i]);
        if (urls[i].endsWith("&nbsp;")) {
          urls[i] = urls[i].slice(0, -1 * "&nbsp;".length);
        }
        if (urls[i].endsWith(".png") || urls[i].endsWith(".jpg") || urls[i].endsWith(".gif")) {
          var newc;
          if (urls[i].slice(1).search(/[Hh][Tt][Tt][Pp][Ss]\:\/\//) == 0) {
            newc = self.getComment().replace(urls[i].slice(1), "<img src='" + urls[i].slice(1) + "' width='480'/>");
          } else {
            newc = self.getComment().replace(urls[i].slice(1), "<img src='" + HTTPS_IMG_PROXY + urls[i].slice(1).replace(/[Hh][Tt][Tt][Pp][sS]?\:\/\//, "") + "' width='480'/>");
          }
          justchanged = true;
          self.editor.setData(newc);
        }

        youtubeThumbnail(urls[i]);
      }
    }
  };

  //Switches youtube URL with the video thumbnail
  var youtubeThumbnail = function(url) {
    var youtubeUrlMatch = url.substr(1).match(youtubeUrl);
    if (youtubeUrlMatch) {
      var videoId = youtubeUrlMatch[1];
      var thumbnailImg = "<img src='http://img.youtube.com/vi/"+ videoId + "/1.jpg'>";
      var thumbnail = self.getComment().replace(youtubeUrlMatch[0], thumbnailImg);
      thumbnails.push([thumbnailImg, videoId]);
      justchanged = true;
      self.editor.setData(thumbnail);
    }
  };

  var editorKeyDown = function(evt) {
    if (evt.data.keyCode == 13) { // enter pressed
      if ($("#enterCheckbox").is(":checked") || shiftOn) {
        setTimeout(APP.CH.addComment, 100);
      }
    } else if (evt.data.keyCode == CKEDITOR.SHIFT + 13) {
      setTimeout(APP.CH.addComment, 100);
    }
  };

    /*
  $('#comment').keydown(function(e) {
    if (e.which == 16) {
      APP.CH.shiftOn = true;
    }
  });

  $('#comment').keyup(function(e) {
    if (e.which == 16) {
      APP.CH.shiftOn = false;
    }
  }); */

  /* Retrieves the copy and pasteable text from the comment */
  this.commentTextOnly = function(cmt) {
    if (cmt == undefined) {
      cmt = self.getComment();
    }
    cmt = cmt.replace(/<\/p><p>/g, "<p>\n</p>");
    cmt = removeTags(cmt);
    cmt = cmt.replace(/&#39;/g, "\'");
    cmt = cmt.replace(/\[br\]/g, "\r\n");
    cmt = cmt.replace(/&nbsp;/g, " ");
    return cmt.replace(/&quot;/g, "\"");
  };
  
  var removeTags = function(toRemove) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = toRemove;
    return tmp.textContent || tmp.innerText || "";
  };

  var addSeparator = function(after) {
    $(".cke_toolbar_separator:first").clone().insertAfter(after);
  };

  var moveAfter = function(source, after) {
    var clone = $(source).clone();
    $(source).remove();
    clone.insertAfter(after);
  };

  var getURLs = function(cmt) {
    return cmt.match(geturl);
  };
  
  var getToolbarElement = function(className) {
    return $( $( className )[0] ).parent().parent();
  };

  this.init(id);
};
