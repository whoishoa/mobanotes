$(function() {// Executes when the page finishes loading
  if (window.devicePixelRatio > 1) {
    $("body").addClass("retina");
    APP.PH.retina = true;
  }
  
  if (navigator.sayswho.indexOf("IE") != -1) {
    $("body").addClass("explorer");
    APP.PH.browser = "explorer";
  } else if (navigator.sayswho.indexOf("Firefox") != -1) {
    $("body").addClass("firefox");
    APP.PH.browser = "firefox";
  } else if (navigator.sayswho.indexOf("Safari") != -1) {
    $("body").addClass("safari");
    APP.PH.browser = "safari";
  } else if (navigator.sayswho.indexOf("Chrome") != -1) {
    $("body").addClass("chrome");
    APP.PH.browser = "chrome";
  } else if (navigator.sayswho.indexOf("Opera") != -1) {
    $("body").addClass("opera");
    APP.PH.browser = "opera";
  } else {
    $("body").addClass("otherbrowser");
    APP.PH.browser = "otherbrowser";
  }
  
  // Set up authentication for all AJAX requests
  $.ajaxSetup({
    headers : {
      'X-Transaction' : 'AJAX',
      'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $( window ).resize(function() {
    APP.resizeHandlers();
  });

});

navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem = ua.match(/\bOPR\/(\d+)/);
        if(tem!= null) return 'Opera '+tem[1];
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();
