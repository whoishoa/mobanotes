/* This method also handles all of the tooltips */
/* Handles all of the comments in the main session and the adding of comments */
function commentsHandler() {
  this.shiftOn = false;
  this.atTop = true;
  this.startPage = null;
  var topMutex = false;
  var addingMutex = false;
  var deletingMutex = false;
  var editingMutex = false;
  var scrollMutex = false;
  var resetHashMutex = false;
  var inBetweenMutex = false;
  var scrollPreserve = 0;
  var CONTENT_ROW_OFFSET = 1;
  var NAV_BAR_SIZE = 35;
  var LOAD_AMOUNT = 50;
  var MONTHS = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "November",
    "December"
  ];

  // Runs at page load
  this.initComments = function() {
  	$('#boardTitle').keypress(function(e) {
	  if (e.which == 13) {
	    APP.PH.switchPage();
      }
    });

    // Neccessary for the overflow scroller for the comments section
    $(".nano").nanoScroller();
    $(".nano").bind("scrolltop", APP.CH.scrolledToTop);
    $(".nano").on("update", APP.CH.handleScroll);
  };

  // TODO: handle when addingMutex is false at the start of this method
  /*
   * This method is executed when the user presses the post button or SHIFT+ENTER or enter
   * to add a new comment to the board.
   */
  this.addComment = function() {
    if ( APP.EH.isWhitespace() ) { // ignore whitespace submits
      return;
    }
    addingMutex = true;
    var message = APP.EH.getFilteredComment();
    message = APP.EH.addVideo(message);
    var comment = {
        board: $("#boardid").val(),
        message: message,
        position: (APP.CH.comments.length == 0) ? 0 : APP.CH.comments[ APP.CH.comments.length - 1 ].id
    };

    APP.WH.sendCommentToServer( comment );
    APP.EH.clearComment();
  };

  /*
   * This method appends all of the comments in APP.CH.comments
   * to the DOM
   */
  var loadAllComments = function() {
    for (var i = 0; i < APP.CH.comments.length; i++) {
      loadComment(i);
    }
  };

  /*
   * This method handles all the new comments that
   * comes from the server.
   */
  this.handleAddCommentResponse = function(data) {
    x = data;
    if (data.comments[0].user_id != APP.UH.userid) { // Only play the sound for non-users
      APP.MH.playSound('assets/sounds/notify/b.mp3');
      APP.UH.popupNotify(APP.UH.userid, data.comments[0].comment);
    }
    if (data.result == "success") {

      if (APP.CH.comments.length == 0) {
        $("#commentsDiv").attr("id", "");
        // Handles the bug where if the comment is really long, you are no longer scrolled at the bottom
        if (isScrolledToBottom()) {
          $("#startOfPost").show();
          scrollToLastComment();
        } else {
          $("#startOfPost").show();
        }
      }

      showNewComments(data);
    } else {
      // Handle failure

    }
  };

  // Deletes the comment with the ID in APP.UH.currTooltipCommentId;
  this.deleteComment = function() {
    $("#settingsEditTooltip").hide();

    deleteMutex = true;
    var comment = {
        board: $("#boardid").val(),
        commentid: APP.UH.currTooltipCommentId,
    };
    APP.WH.deleteCommentFromServer( comment );
  };
  
  this.commitEditComment = function(id) {
    
    var currEditor = APP.CommentEditors[id];
    
    if ( currEditor.isWhitespace() ) { // ignore whitespace submits
      return;
    }
    addingMutex = true;
    var message = currEditor.getFilteredComment();
    message = currEditor.addVideo(message);
    var comment = {
        board: $("#boardid").val(),
        message: message,
        position: id
    };

    APP.WH.editCommentInServer( comment );
    currEditor.clearComment();
    
  };

  this.handleDeletedCommentResponse = function(data) {
    var commentId = parseInt(data.deletedid);
    var needAdjusting = $("#comment" + commentId).position().top < 0;
    if (APP.CH.getCommentIndex(data.deletedid) !== false) {
      if (needAdjusting) {
        preserveScrollPosition();
      }
      $("#comment" + commentId).remove();

      // Update the css styles of the comment above and below the removed comment
      var between = getBetween(commentId);
      APP.CH.comments.splice(APP.CH.getCommentIndex(commentId),1);

      adjustComment( APP.CH.getCommentIndex(between[0]) );
      if (between[1] != "max") {
        adjustComment( APP.CH.getCommentIndex(between[1]) );
      }
      if (needAdjusting) {
        restoreScrollPosition();
      }
    }
  };

  this.handleEditedCommentResponse = function(data) {
    APP.CH.cancelEditComment(data.edited.id);
    var c = $("#commentContent" + data.edited.id);
    if (c.length != 0) {
      c.html(data.edited.comment);
    }
  };

  /*
   * This method loads more comments when the user scrolled to the top of the page
   * or has clicked a "Load more posts" button
   */
  this.scrolledToTop = function(e) {
    currentlyLoadingMore();
    if (! this.atTop && !topMutex) {
      topMutex = true;

      APP.WH.requestTopComments({position: APP.CH.comments[0].id, reverse: "yes", board: $("#boardid").val()},
          handleLoadNewTopCommentsResponse, handleLoadNewTopCommentsFail);
    }
  };

  /*
   * This method hides the "Load More Posts" div
   * and signals that all of the top posts has been loaded.
   */
  var hideLoadingMore = function() {
    this.atTop = true;
    $("#loadingMorePosts").hide();
  };

  /*
   * This method shows the "Load More Posts" div
   * and signals that there are more posts to load.
   */
  var showLoadingMore = function() {
    this.atTop = false;
    $("#loadMorePostsButton").show();
    $("#loadingMorePostsDiv").hide();
    $("#loadingMorePosts").addClass("loadMoreButton commentsButton");
    $("#loadingMorePosts").attr("onclick", "APP.CH.scrolledToTop()");
  };

  /*
   * This method replaces the "Load More Posts" div with a ajax loading
   * gif to show that posts are currently being loaded.
   */
  var currentlyLoadingMore = function() {
    $("#loadMorePostsButton").hide();
    $("#loadingMorePostsDiv").show();
    $("#loadingMorePosts").removeClass("loadMoreButton commentsButton");
    $("#loadingMorePosts").attr("onclick", "");
  };

  /*
   * Use this method if you want to load comments upward of
   * the current comment you are viewing.
   *
   * Usage:
   * preserveScrollPosition();
   * // code that will change the scroll position
   * restoreStrollPosition();
   */
  var preserveScrollPosition = function() {
    var scroller = $("#commentsOverflowContainer .nano-content").get(0);
    scrollPerserve = scroller.scrollHeight - scroller.scrollTop;
  };

  /*
   * This method is called to restore the preserved scroll position
   */
  var restoreScrollPosition = function() {
    var scroller = $("#commentsOverflowContainer .nano-content").get(0);
    scroller.scrollTop = scroller.scrollHeight - scrollPerserve;
  };

  /*
   * This method handles the server response when the client
   * requests more posts by scrolling to the top or pressing the
   * "Load more posts" button
   */
  var handleLoadNewTopCommentsResponse = function(data) {
    if (data.result == "success") {
      preserveScrollPosition();
      showNewTopComments(data);
      if (data.end) {
        hideLoadingMore();
      } else {
        showLoadingMore();
      }
      restoreScrollPosition();
    } else {
      // TODO: Handle failure

    }
    topMutex = false;
  };

  /*
   * This method formats the comment to look correctly
   */
  var formatComment = function(cmt) {
    cmt = cmt.replace(/&#39;/g, "\'");
    cmt = cmt.replace(/\[br\]/g, "<br />");
    return cmt.replace(/&quot;/g, "\"");
  };

  /*
   * This method determines if the two index corresponds to comments by the same
   * user on the same browser.
   */
  var sameUser = function(index1, index2) {
    return APP.CH.comments[index1].user == APP.CH.comments[index2].user &&
        APP.CH.comments[index1].color == APP.CH.comments[index2].color;
  };

  /*
   * This method updates the commentSolo, commentTop, commentMiddle,
   * and commentBottom for the comment that the specified index.
   */
  var adjustComment = function(index) {
    var cmt = APP.CH.comments[index];
    var first = true;
    if (index > 0 && sameUser(index - 1, index)) {
      first = false;
    }
    var next = false;
    if (index < APP.CH.comments.length - 1 && sameUser(index + 1, index)) {
      next = true;
    }

    var contentRow = $(".contentRow:nth(" + (index + CONTENT_ROW_OFFSET) +")");

    contentRow.removeClass("commentSolo commentTop commentMiddle commentBottom");

    var commentUserHolderSelector = ".contentRow:nth(" + (index + CONTENT_ROW_OFFSET) +") .mainLeft";
    if (!first) {
      $(commentUserHolderSelector).html("&nbsp;");
    } else {
      $(commentUserHolderSelector).html( $(".commentUser:nth(1)").clone() );
      $(commentUserHolderSelector + " .commentUser").css("background-color", "#" + cmt.color);
      $(commentUserHolderSelector + " .commentUser").css("border", "1px solid #" + cmt.color);
    }

    if (first && !next) {
      contentRow.addClass("commentSolo");
    } else if (first && next) {
      contentRow.addClass("commentTop");
    } else if (!first && next) {
      contentRow.addClass("commentMiddle");
    } else {
      contentRow.addClass("commentBottom");
    }
  };

  /* Converts the server time string into the format "12:32 PM" */
  var getTimeString = function(time) {
    var day = new Date(Date.parse(time));
    var minute = "" + day.getMinutes();
    if (minute < 10) {
      minute = "0" + minute;
    }
    var pm = "AM";
    var hour = day.getHours();
    if (hour >= 12) {
      hour = hour % 12;
      pm = "PM";
    }
    if (hour == 0) {
      hour = 12;
    }
    var date = hour + ":" + minute + " " + pm;
    return date;
  };

  /* Converts the server time string into the format "March 21, 2015" */
  var getDateString = function(time) {
    var day = new Date(Date.parse(time));
    var d = day.getDate();
    var ds = "th";
    if (d == 1) {
      ds = "st";
    } else if (d == 2) {
      ds = "nd";
    }
    return MONTHS[day.getMonth()] + " " + d + ds + ", " + day.getFullYear();
  };

  /* This method adds a comment to the end of the comments HTML Dom. */
  var loadComment = function(index) {
    $(".contentRow:first").clone().insertAfter(".contentRow:last");
    loadCommentCore(index, ".contentRow:last");
  };

  /* This method adds a comment to the start of the comments HTML DOM */
  var loadTopComment = function(index) {
    var selector = ".contentRow:nth(" + (CONTENT_ROW_OFFSET + index)+ ")";
    $(".contentRow:first").clone().insertAfter( ".contentRow:nth(" + (CONTENT_ROW_OFFSET - 1 + index)+ ")" );
    loadCommentCore(index, selector);
  };

  /*
   * Private version: This method physical adds the html code for a particular comment
   * to the DOM
   */
  var addNewComment = function(cmt) {
    var scroll = false;
    if (isScrolledToBottom()) {
      scroll = true;
    }
    APP.CH.comments.push({
      "id" : cmt.id,
      "color" : cmt.color,
      "comment" : cmt.comment,
      "time" : cmt.created_at,
      "user" : cmt.user_id
    });
    if (APP.CH.comments.length > 1) {
      adjustComment(APP.CH.comments.length - 2);
    }
    loadComment(APP.CH.comments.length - 1);
    if (scroll) {
      scrollToLastComment();
    }
  };

  /*
   * This method adds the button that shows:
   * Load the next 50 posts
   * or
   * Load the previous 50 posts.
   */
  var handleBetweenEnd = function(data, index, more) {
    var selector = ".contentRow:nth(" + (CONTENT_ROW_OFFSET + index)+ ")";
    var clone = $(".loadBetweenRow:first").clone();
    var startid = APP.CH.comments[index - 1].id + 1;
    var endid = APP.CH.comments[index].id - 1;
    clone.attr("id", "loadBetween-" + startid);
    clone.attr("start", startid);
    clone.attr("end", endid);
    clone.removeClass("hidden").addClass("row");
    clone.find(".betweenMessageNumber").text(more + " ");

    if (more < LOAD_AMOUNT) {
      clone.find(".betweenMessageButtonNumber").text(more + " ");
    }
    if (more == 1) {
      clone.find(".plural").text("");
    }
    clone.find(".nextBetween").attr("onclick", "APP.CH.loadMoreBetween(" + startid + ", false, " + endid + ")");
    clone.find(".prevBetween").attr("onclick", "APP.CH.loadMoreBetween(" + startid + ", true, " + endid + ")");
    clone.insertAfter( ".contentRow:nth(" + (CONTENT_ROW_OFFSET - 1 + index)+ ")" );
  };

  /*
   * This method manually adds the between comments
   * retrieved from the loadBetween server handler.
   */
  var addBetweenComments = function(data) {
    if (data.method == 2) {
      preserveScrollPosition();
    }

    if (data.comments.length > 0) {
      var addStart = getBetween(data.comments[0].id);
      addStart[2] += 1;

      for (var i = 0; i < data.comments.length; i++) {
        var cmt = data.comments[i];
        APP.CH.comments.splice(addStart[2] + i, 0, {
          "id" : cmt.id,
          "color" : cmt.color,
          "comment" : cmt.comment,
          "time" : cmt.created_at,
          "user" : cmt.user_id
        });
      }
      for (var i = addStart[2]; i < addStart[2] + data.comments.length; i++) {
        loadTopComment(i);
      }

      // remove the unneccessary one
      if (addStart[2] - 1 >= 0) {
        var btwn = $("#loadBetween-" + (APP.CH.comments[addStart[2] - 1].id + 1));
        if (btwn.length > 0) {
          btwn.remove();
        }
      }

      // Show the load previous and load next post buttons
      if (data.upend && addStart[2] == 0) {
        hideLoadingMore();
      } else if (!data.upend && addStart[2] != 0) {
        handleBetweenEnd(data, addStart[2], data.upmore);
      } else if (data.upend && addStart[2] != 0){
        adjustComment(addStart[2] - 1);
      }
      if (!data.downend) {
        handleBetweenEnd(data, addStart[2] + data.comments.length, data.downmore); // insert before this element
      } else {
        adjustComment(addStart[2] + data.comments.length);
      }
    }

    if (data.method == 1) { // reposition
      
      APP.CH.scrollIntoView(data.tofind);
    } else if (data.method == 2) {
      restoreScrollPosition();
    }
  };

  /* Adds the new comments retrieved by the server from
   * the request for more top comments to the DOM */
  var showNewTopComments = function(data) {
    if (data.comments.length > 0) {
      for (var i = 0; i < data.comments.length; i++) {
        var cmt = data.comments[i];
        APP.CH.comments.splice(i, 0, {
          "id" : cmt.id,
          "color" : cmt.color,
          "comment" : cmt.comment,
          "time" : cmt.created_at,
          "user" : cmt.user_id
        });
      }

      for (var i = 0; i < data.comments.length; i++) {
        loadTopComment(i);
      }
      adjustComment(data.comments.length);
    }

  };

  /* Adds the new comments retrieved by the server to the DOM */
  var showNewComments = function(data) {
    if (data.comments.length > 0) {
      for (var i = 0; i < data.comments.length; i++) {
        addNewComment(data.comments[i]);
      }
    }
  };

  /* Handler when the server fails to return more top comments */
  var handleLoadNewTopCommentsFail = function(data) {
    topMutex = false;
  };

  // Scroll to the end of the page
  var scrollToLastComment = function() {
    window.location = "#";
    $("#commentsBottomFiller").get(0).scrollIntoView();
  };

  /*
   * This method is executed when the user scrolls on the comments.
   * This method should change isScrolledToBottom to the correct value.
   */
  var isScrolledToBottom = function() {
    var content = $("#commentsOverflowContainer .nano-content").get(0);
    return content.scrollHeight == content.clientHeight + content.scrollTop;
  };

  /* Updates the URL after strolling */
  var onScrollAfterDelay = function() {
    if (isScrolledToBottom()) {
      window.location = "#";
      scrollMutex = false;
      return;
    }
    // update the position
    var min = 0;
    var max = APP.CH.comments.length - 1;
    var content = $("#commentsOverflowContainer .nano-content").get(0);
    if (max <= 1) {
      return;
    } else {
      var curr = Math.floor(max / 2);
      var currMin = max;
      while (min <= max) {
        var elem = $("#comment" + APP.CH.comments[curr].id);
        if (  elem.offset().top < NAV_BAR_SIZE) {
          min = curr + 1;
        } else {
          currMin = curr;
          max = curr - 1;
        }
        curr = Math.floor((min + max) / 2) ;
      }
      APP.PH.resetMutex = true;
      window.location = "#" + APP.CH.comments[currMin].id;
      setTimeout(function() { APP.PH.resetMutex = false; }, 100);
    }
    scrollMutex = false;
  };

  /* This method is executed whenever the user scrolls */
  this.handleScroll = function(event, vals) {
    if (! scrollMutex) {
      scrollMutex = true;
      setTimeout(onScrollAfterDelay, 2000);
    }
  };

  /*
   * Show the comment with the id of page as the first comment in the
   * user's scroll window.
   */
  this.scrollIntoView = function(page) {
    if (page == null) {
      scrollToLastComment();
      return;
    }
    if (APP.CH.getCommentIndex(page) !== false) {
      var elem = $("#comment" + page);
      var content = $("#commentsOverflowContainer .nano-content").get(0);
      content.scrollTop = content.scrollTop + elem.offset().top - NAV_BAR_SIZE - 5;
    } else {
      var between = getBetween(page);
      if (between[3] == "max") {
        scrollToLastComment();
      } else {
        APP.CH.scrollIntoView(between[1]);
      }
    }

  };

  /*
   * This method handles all of the grunt work of adding all the HTML specific
   * stuff to the DOM for a comment.
   */
  var loadCommentCore = function(index, selector) {
    var cmt = APP.CH.comments[index];
    $(selector).attr("id", "comment" + cmt.id);
    $(selector + " .mainMiddleComment .restrictSelect:first-child").attr("id", "commentContent" + cmt.id);
    $(selector + " .mainMiddleComment .restrictSelect:first-child").html( formatComment(cmt.comment) );
    $(selector + " .mainRight").css("color", "#" + cmt.color);
    $(selector + " .mainRight .timeCell").text( getTimeString(cmt.time) );
    $(selector + " .mainRight .timeCell").attr("time", getDateString(cmt.time) );

    adjustComment(index);

    $(selector).css("display", "table-row" );

    $(selector + " .commentsIcon:first").on('mouseover', APP.UH.clipboardIconOnHandler);
    $(selector + " .commentsIcon:first").on('mouseout', APP.UH.clipboardIconOffHandler);
    $(selector + " .commentsIcon:first").on('click', APP.UH.clipboardIconClickHandler);
    $(selector + " .commentsIcon:first").attr("data-clipboard-text", APP.EH.commentTextOnly(cmt.comment) );

    var client = new ZeroClipboard( $(selector + " .commentsIcon:first").get() );
    client.on( "ready", function( readyEvent ) { } );

    $(selector + " .settingsIcon").on('mouseover', APP.UH.settingsIconOnHandler);
    $(selector + " .settingsIcon").on('mouseout', APP.UH.settingsIconOffHandler);
    $(selector + " .settingsIcon").on('click', APP.UH.settingsIconClickHandler);

    $(selector + " .timeCell").on('mouseover', APP.UH.timeOnHandler);
    $(selector + " .timeCell").on('mouseout', APP.UH.timeOffHandler);
  };

  /*
   * Displays the comments that were loaded with the webpage
   */
  this.initBasicComments = function(page) {
    loadAllComments();
    // setInterval(loadNewComments, 1000);
    this.scrollIntoView(page);

    if (this.comments.length > 0) {
      $("#startOfPost").show();
      if (! this.atTop) {
        $("#loadingMorePosts").show();
      }
    }
    
    this.scrollIntoView(page);
    APP.CH.startPage = page;
  };

  /*
   * This method is called when the user clicks on Load next 50 posts
   * or Load previous 50 posts (whetherReverse = true).
   * startid and endid is the range of id missing from the board
   */
  this.loadMoreBetween = function(startid, whetherReverse, endid) {
    if (whetherReverse) {
      preserveScrollPosition();
    }
    var clone = $("#loadBetween-" + startid);
    clone.find(".loadingBetweenDiv").show();
    clone.find(".betweenCommon").hide();
    if (whetherReverse) {
      restoreScrollPosition();
    }

    APP.CH.loadBetween(null, startid, endid, whetherReverse);
  };

  /*
   * This method loads the comments between the page
   * and focuses the post with that page id.
   * This method is called by the Page Handler when
   * the user changes the URL to include a post id that hasn't been
   * loaded.
   */
  this.loadBetween = function(page, startid, endid, whetherReverse) {
    var betweenSuccess = function(data) {
      addBetweenComments(data);
      inBetweenMutex = false;
    };

    var betweenFail = function(data) {
      inBetweenMutex = false;
    };
    if (inBetweenMutex) {
      return;
    }
    inBetweenMutex = true;

    if (startid != null) {
      var request = {
        uplimit: startid,
        downlimit: endid,
        tofind: startid,
        method: (whetherReverse) ? 2 : 3,
        board: $("#boardid").val()
      };
    } else {
      var between = getBetween(page);
      var request = {
        uplimit: between[0],
        downlimit: between[1],
        tofind: page,
        method: 1,
        board: $("#boardid").val()
      };
    }

    sent = request;
    APP.WH.getIntervalComments(request, betweenSuccess, betweenFail);
  };

  // Returns an array of [oneSmaller, oneBigger] where both elements
  // are comments IDs. Returns "max" as oneBigger if the page is bigger
  // than all the other ids in the array or if the array is empty.
  var getBetween = function(page) {
    var min = 0;
    var max = APP.CH.comments.length - 1;
    if (max < 0) {
      return [-1, "max"];
    } else {
      var curr = Math.floor(max / 2);
      var currMin = -1;
      var currMax = "max";
      while (min <= max) {
        if (APP.CH.comments[curr].id == page) {
          return [(curr - 1 >= 0) ? APP.CH.comments[curr - 1].id : -1,
              (curr + 1 < APP.CH.comments.length) ? APP.CH.comments[curr + 1].id : "max"];
        } else if (APP.CH.comments[curr].id < page) {
          currMin = curr;
          min = curr + 1;
        } else {
          currMax = curr;
          max = curr - 1;
        }
        curr = Math.floor((min + max) / 2) ;
      }
      return [
          (currMin != -1) ? APP.CH.comments[currMin].id : -1,
          (currMax != "max") ? APP.CH.comments[currMax].id : "max", currMin, currMax
      ];
    }
  };

  /*
   * This method gets the comment with the specified id. If
   * that comment doesn't exist, this method returns false.
   */
  this.getCommentIndex = function(id) {
    var min = 0;
    var max = APP.CH.comments.length - 1;
    if (max < 0) {
      return false;
    } else {
      // binary search
      var curr = Math.floor(max / 2);
      while (min <= max) {
        if (APP.CH.comments[curr].id == id) {
          return curr;
        } else if (APP.CH.comments[curr].id < id) {
          min = curr + 1;
        } else {
          max = curr - 1;
        }
        curr = Math.floor((min + max) / 2) ;
      }
      return false;
    }
  };

  // Takes in a dom element and returns the id of the
  // comment. Will break if it is not apart of an comment.
  this.getCommentElementId = function(element) {

    while (! element.classList.contains("mainRow")) {
      element = element.parentElement;
    }
    return parseInt(element.getAttribute("id").replace("comment", ""));
  };

  // Edits the comment with the ID in APP.UH.currTooltipCommentId;
  this.editComment = function() {
    $("#settingsEditTooltip").hide();
    var commentId = APP.UH.currTooltipCommentId;
        $("#commentContent" + commentId).parent().contents().unwrap();

    function afterLoad(obj) {
      $(obj.editorId).addClass("reeditEditor");
      var c = $("#reeditEditorBotTemplate").clone();
      c.attr("id", "");
      c.find(".alternateButton").attr("onclick", "APP.CH.cancelEditComment(" + commentId + ");");
      c.find(".primaryButton").attr("onclick", "APP.CH.commitEditComment(" + commentId + ");");
      $(obj.editorId).append(c);
    }

    APP.CommentEditors[commentId] = new editorHandler("commentContent" + commentId, afterLoad);
  };
  
  this.cancelEditComment = function(id) {
    var c = $("#commentContent" + id);
    if (c.length != 0 && ! c.parent().hasClass("mainMiddleComment")) {
      c = c.clone();
      var container = $(".contentRow:first .mainMiddleComment").clone();
      container.html("");
      container.append(c);
      var p = $("#commentContent" + id).parent();
      p.html("");
      p.append(container);
      c.show();
      c.css("visibility", "visible");
    }
  };

};
