
/* This object handles everything that has to do with the media objects like Youtube videos, Images, and Sounds */
function mediaHandler() {
  // switches to true when the user posts and switchs to off when the user receives
  // the post from the server. This prevents the sound from playing for that user.
  this.sentMutex = false;
  
  var music = false;
  
  /*
   * Sets and plays the sound at the specified file location.
   */
  this.playSound = function(name) {
    APP.MH.setSound(name);
    APP.MH.replay();
  };
  
  /*
   * Specifies the sound file location for the sound to be played
   * at the next call to replay()
   */
  this.setSound = function(name) {
    music = new Audio(name);
    // music2 = new Audio(name.replace(".wav", ".mp3"));
    // music.addEventListener('complete', onSoundComplete);
  };
  
  /*
   * Precondition: The webpage has exactly one embedded sound set using 
   *     setSound or playSound.
   * Replays the mainsound of the webpage.
   */
  this.replay = function(){
  	music.play();
  	// music.onended = function() { onSoundComplete() };
  };
  
};
