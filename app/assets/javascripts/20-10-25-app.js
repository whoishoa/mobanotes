// JavaScript Document

/*
 * File: app.js
 * Description: The purpose of this file is to control
 *     the rendering of different page views on the app
 *     in order to create a unified app experience.
 */

/*
 * The purpose of this object is to manage and keep track of the state
 * of the current webpage.
 * You must call the variable you instantiate this group to as "APP".
 */
function boardApp() {
  // Tracks whether shift has been pressed in the comment textArea
  this.PH = new pageHandler();
  this.UH = new userHandler();
  this.CH = new commentsHandler();
  this.SH = new searchHandler();
  this.EH;
  this.CommentEditors = {};
  this.MH = new mediaHandler();
  this.appType = "publicboard";
  
  // Called whenever the URL is changed.
  this.urlChanged = function() {
    APP.PH.urlSetUp();
  };
  
  // Initiates the handlers that needs to be initiated after the page loads
  var loadHandlers = function() {
    APP.WH = new websocketHandler();
  };
  
  // Runs the starting code for each handler
  var initHandlers = function() {
    APP.CH.initComments();
    APP.EH = new editorHandler("comment");
    APP.PH.initPage();
    APP.UH.initUser();
  };
  
  // Called when the website has been completely loaded.
  this.start = function() {
    APP.UH.loadColors($("#startColor").val());
    loadHandlers();
    initHandlers();
    
  };
  
  this.resizeHandlers = function() {
    APP.EH.resized();
  };
  
}

var APP = new boardApp();
