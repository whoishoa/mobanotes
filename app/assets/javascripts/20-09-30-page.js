/* This method handles everything that has to do with the URL and user's computer */
function pageHandler() {
  this.retina = false;
  this.browser = "chrome";
  // resetMutex makes sure goToPage() is not activated when the url is changed by a javascript script.
  this.resetMutex = false;
  this.currPage = "";
  
  /* Gets everything following the hash and before the '?' in the URI */
  var getPageURI = function() {
     return (window.location.hash.indexOf("?") == -1) ? window.location.hash.substr(1) : window.location.hash.substr(1, window.location.hash.indexOf("?") - 1 );
  };
  
  /* 
   * This function checks the URL and sets of the page according
   * to the url.
   */
  this.urlSetUp = function() {
    if (APP.PH.resetMutex) {
      APP.PH.resetMutex = false;
      return;
    }
    var page = getPageURI();
    if (page == "" || page == APP.PH.currPage) {
      // do nothing
    } else {
      goToPage(page);
    }
  };
  
  /*
   * This method is called when the user wants to go to a
   * comment that hasn't been loaded yet.
   */
  var goToPage = function(page) {
    if ($("#comment" + page).length == 0) {
      APP.CH.startPage = page;
      APP.CH.loadBetween(page);
    } else {
      APP.CH.startPage = page;
      APP.CH.scrollIntoView(page);
    }
  };
  
  /* This method occurs when the boardTitle text field been blurred or an enter has been clicked. */
  this.switchPage = function() {
    var newPage = $("#boardTitle").val().replace(/\W/g, '').toLowerCase();
    if (newPage != $("#board").val()) {
      window.location = "/" + newPage;
    } else {
      $("#boardTitle").val(newPage);
    }
  };
  
  /*
   * This method is called at the start of the webpage.
   */
  this.initPage = function() {
    var page = getPageURI();
    APP.CH.initBasicComments();
    if (page != "") {
      goToPage(page);
    }
  };
  
  
};
