/* Everything that needs to be transfered between the client and the webserver occurs here */
function websocketHandler() {
  if (window.location.host.indexOf("mobanotes") != -1) {
    var dispatcher = new WebSocketRails(window.location.host + ':3001/services/websocket');
  } else {
    var dispatcher = new WebSocketRails(window.location.host + '/services/websocket');
  }
  var channel = dispatcher.subscribe( $("#board").val() );
  
  // Receives new comment messages from the server
  channel.bind('new_comment', APP.CH.handleAddCommentResponse);
  channel.bind('deleted_comment', APP.CH.handleDeletedCommentResponse);
  channel.bind('edited_comment', APP.CH.handleEditedCommentResponse);
  
  this.sendCommentToServer = function(comment) {
    dispatcher.trigger(APP.appType + '.add_message', comment);
  };
  
  this.deleteCommentFromServer = function(comment) {
    dispatcher.trigger(APP.appType + ".delete_message", comment);
  };
  
  this.editCommentInServer = function(comment) {
    dispatcher.trigger(APP.appType + ".edit_message", comment);
  };
  
  this.requestTopComments = function(request, successHandler, failureHandler) {
    dispatcher.trigger(APP.appType + ".load_posts", request, successHandler, failureHandler);
  };
  
  this.getIntervalComments = function(request, successHandler, failureHandler) {
    dispatcher.trigger(APP.appType + ".interval_posts", request, successHandler, failureHandler);
  };
  
  // Users requests
  
  this.requestNewColor = function(successHandler, failureHandler) {
    dispatcher.trigger("change_color", {}, successHandler, failureHandler);
  };
  
  this.updateEnter = function(request, successHandler, failureHandler) {
    dispatcher.trigger("toggle_enter", request, successHandler, failureHandler);
  };
  
};
