/* This object handles everything that has to do with searches */
function searchHandler() {
  
  var currentSearchSelection = "this website";
  var changeSearchOn = false;
  var SEARCH_OPTIONS = [
    "this website",
    "this page"
  ];
  
  
  /*
   * This method is executed when the user initiates a search
   * by either clicking on the search icon or clicking on the
   * enter sign in the search field.
   */
  this.search = function() {
    
  };
  
  /*
   * This method is executed when the user clicks on the search
   * dropdown menu.
   */
  this.changeSearch = function() {
    if (changeSearchOn) {
      APP.SH.changeSearchOff();
    } else {
      if ( $(".searchOption").length != SEARCH_OPTIONS.length - 1 ) {
        // set up the right number of cells
        for (var i = 2; i < SEARCH_OPTIONS.length; i++) {
          $(".searchOption:first").clone().insertAfter(".searchOption:last");
          $(".searchOption:last").attr("id", "searchOption" + i);
        } 
        
        for (var i = 1; i < SEARCH_OPTIONS.length; i++) {
          $("#searchOption" + i + " .cell").text(SEARCH_OPTIONS[i]);
          $("#searchOption" + i + " .cell").attr("onclick", "APP.SH.searchOption('" + SEARCH_OPTIONS[i] + "')");
        }
      }
      
      $("#searchOption").show();
      changeSearchOn = true;
    }
  };
  
  /*
   * This method is executed when the user clicks on a new option
   * in the search menu options list to select that option.
   */
  this.searchOption = function(changeTo) {
    $("#selectedSearchOption").text(changeTo);
    
    var indi = 1;
    for (var i = 0; i < SEARCH_OPTIONS.length; i++) {
      if (SEARCH_OPTIONS[i] != changeTo) {
        $("#searchOption" + indi + " .cell").text(SEARCH_OPTIONS[i]);
        $("#searchOption" + indi + " .cell").attr("onclick", "APP.SH.searchOption('" + SEARCH_OPTIONS[i] + "')");
        indi ++;
      }
    }
    
    APP.SH.changeSearchOff();
  };
  
  /* This method hides the search options list */
  this.changeSearchOff = function() {
    $("#searchOption").hide();
    changeSearchOn = false;
  };
  
};
