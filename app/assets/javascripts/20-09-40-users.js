
/* This object handles everything that has to do with the User, colors, and tooltips */
function userHandler() {
  this.userid = false;
  this.currTooltipCommentId = false;
  this.notifiable = false;
  var enterMutex = false;
  var colorMutex = false;
  var clipboardTooltipMutex = false;

  this.initUser = function() {
    APP.UH.userid = parseInt($("#userid").val());

    APP.UH.handleHideOutsides(false);
    
    if (Notification.permission == "default") {
      Notification.requestPermission(function() {
        APP.UH.notifiable = Notification.permission == "granted";
      });
    } else {
      APP.UH.notifiable = Notification.permission == "granted";
    }
  };
  
  // This method holds all the hideOutside handlers.
  // immediateHide - false if you want to create the onClickOutside listeners, 
  //                 true if you want to immediately hide everything that should be hidden on an onClickOutside event
  this.handleHideOutsides = function(immediateHide) {
    hideOutside("#settingsEditTooltip", ".settingsIcon", function() {}, immediateHide);
    hideOutside(".moreMenuContainer", "#menuButtonDiv", function() {
      hideMoreMenu();
    }, immediateHide);
    hideOutside("#searchOption", "#searchMiddleArrow, #selectedSearchOption", function() { 
      APP.SH.changeSearchOff();
    }, immediateHide);
  };

  /*
   * This method is called whenever the user clicks on the checkbox
   * with the text "press enter to post".
   */
  this.toggleEnter = function() {
    var handleNewEnterResponse = function(data) {
      enterMutex = false;
    };
    if (enterMutex) {
      return;
    }
    enterMutex = true;

    APP.WH.updateEnter( {"newvalue": ($("#enterCheckbox").is(":checked")) ? 1 : 0 },
        handleNewEnterResponse,
        function() {
            enterMutex = false;
        }
    );
  };

  /*
   * This method is executed when the user clicks on the anonymous icon to change
   * their color
   */
  this.newColor = function() {
    var handleNewColorResponse = function(data) {
      APP.UH.loadColors(data.color);
      colorMutex = false;
    };
    if (colorMutex) {
      return;
    }
    colorMutex = true;
    APP.WH.requestNewColor(handleNewColorResponse, function() { colorMutex = false; });
  };

  /*
   * This method changes the color scheme to a particular color
   */
  this.loadColors = function(color) {
    $(".userBackgroundColor").css("background-color", "#" + color);
    $(".userBorderColor").css("border", "1px solid #" + color);
    $(".userBorderBottomColor").css("border-bottom", "1px solid #" + color);
    $(".userBorderTopColor").css("border-top", "1px solid #" + color);
    $(".userBorderLeftColor").css("border-left", "1px solid #" + color);
    $(".userBorderRightColor").css("border-right", "1px solid #" + color);
    $(".userTextColor").css("color", "#" + color);
    $(".moreMenuContainer").css("color", "#" + color);
    $(".moreMenuContainer").css("border-color", "#" + color);
  };

    /* This method moves the clipboard tooltip to the correct location and shows it */
  this.clipboardIconOnHandler = function(e) {
    clipboardTooltipMutex = true;
    $("#clipboardTooltip").css("top", $(e.target).position().top + 63);
    if (APP.PH.browser == "safari") {
      $("#clipboardTooltip").css("left", $(e.target).position().left - 87);
    } else {
      $("#clipboardTooltip").css("left", $(e.target).position().left - 92);
    }
    $("#clipboardTooltip").show();
  };

  /* This method moves the clipboard tooltip to the correct location and shows it */
  this.clipboardIconClickHandler = function(e) {
    $("#copiedTooltip").css("top", $(e.target).position().top + 63);
    $("#copiedTooltip").css("left", $(e.target).position().left - ($("#copiedTooltip").width() / 2) + ($(e.target).width() / 2));

    $("#clipboardTooltip").hide();
    $("#copiedTooltip").show();
  };

  /* This method hides the clipboard tooltip on mouse out */
  this.clipboardIconOffHandler = function(e) {
    clipboardTooltipMutex = false;
    setTimeout(function() {
      if (!clipboardTooltipMutex) {
        $("#clipboardTooltip").hide();
        $("#copiedTooltip").hide();
      }
    }, 100);

  };
  
  /* This method moves the clipboard tooltip to the correct location and shows it */
  this.settingsIconClickHandler = function(e) {
    if (APP.UH.currTooltipCommentId == APP.CH.getCommentElementId(e.target) && $("#settingsEditTooltip").is(":visible")) {
      APP.UH.currTooltipCommentId = false;
      $("#settingsEditTooltip").hide();
    } else {
      APP.UH.currTooltipCommentId = APP.CH.getCommentElementId(e.target);
      $("#settingsEditTooltip").css("top", $(e.target).position().top + 63);
      $("#settingsEditTooltip").css("left", $(e.target).position().left - ($("#copiedTooltip").width() / 2) + ($(e.target).width() / 2) + 1);
      $("#settingsTooltip").hide();
      $("#settingsEditTooltip").show();
    }
    
  };

  /* This method moves the settings tooltip to the correct location and shows it */
  this.settingsIconOnHandler = function(e) {
    var tooltip = $("#settingsTooltip");
    if ( APP.UH.currTooltipCommentId != APP.CH.getCommentElementId(e.target) ) {
      tooltip.css("top", $(e.target).position().top + 63);
      tooltip.css("left", $(e.target).position().left - 29);
      tooltip.show();
    }
  };

  /* This method hides the settings tooltip on mouse out */
  this.settingsIconOffHandler = function(e) {
    $("#settingsTooltip").hide();
  };

  /* This method moves the time tooltip to the correct location and shows it */
  this.timeOnHandler = function(e) {
    $("#dateTooltip .tipCell").text($(e.target).attr("time"));

    $("#dateTooltip").css("top", $(e.target).position().top + 63);
    $("#dateTooltip").css("left", $(e.target).position().left - 30);
    $("#dateTooltip").show();
  };

  /* This method hides the time tooltip on mouse out */
  this.timeOffHandler = function(e) {
    $("#dateTooltip").hide();
  };

  /* This method shows the tooltip that tells the user how to change theme colors */
  this.colorTooltipShow = function() {
    $("#colorTooltip").css("top", $("#footerUser img").position().top - 38);
    $("#colorTooltip").css("left",
        $("#footerUser img").position().left - ($("#colorTooltip").width() / 2) + ($("#footerUser img").width() / 2) + 1);
    $("#colorTooltip").show();
  };

  /* This method shows the tooltip that tells the user about SHIFT+ENTER */
  this.enterTooltipShow = function() {
    $("#enterTooltip").css("top", $("#enterCheckbox").position().top - 35);
    if (APP.PH.browser == "safari") {
      $("#enterTooltip").css("left", $("#enterCheckbox").position().left - 150);
    } else {
      $("#enterTooltip").css("left", $("#enterCheckbox").position().left - 157);
    }

    $("#enterTooltip").show();
  };

  // Hides a tooltip with the correct id structure
  this.tooltipHide = function(which) {
    $("#" + which + "Tooltip").hide();
  };

  // Shows the more menu
  this.showMoreMenu = function() {
    if ($(".moreMenuContainer").is(":visible")) {
      hideMoreMenu();
    } else {
      $(".moreMenuContainer").show();
      $("#menuButtonDiv").addClass("active shaded");
    }
  };
  
  this.popupNotify = function(username, msg) {
    var nt = new Notification(username + ":", { body: msg, icon: 'assets/mobanotesfavicon.png'});
    setTimeout(function() { nt.close(); }, 5000);
  };
  
  // hides the more menu
  var hideMoreMenu = function() {
    $(".moreMenuContainer").hide();
    $("#menuButtonDiv").removeClass("active shaded");
  };

  // Hides the select object when the user clicks outside that object.
  // Must only be used in the handleHideOutsides method.
  // selector - is the selector of the item to hide when it is clicked outside of
  // ignore - a selector where if the object associated with ignore is clicked on, the onClickOutside
  //          event does not occur
  // afterHideCallback - executed at the end of an onClickOutside event.
  var hideOutside = function(selector, ignore, afterHideCallback, immediateHide) {
    var hideSelector = function(selector, afterHideCallback) {
      $(selector).hide();
      if (typeof afterHideCallback !== "undefined") {
        afterHideCallback();
      }
    };
    if (immediateHide) {
      hideSelector(selector, afterHideCallback);
      return;
    } 
    $(document).mouseup(function (e){
      var container = $(selector);
      var clickedOutside = !container.is(e.target) && container.has(e.target).length === 0;
      if (typeof ignore !== "undefined") {
        var ignoreContainer = $(ignore);
        clickedOutside = clickedOutside && !ignoreContainer.is(e.target) && ignoreContainer.has(e.target).length === 0;
      } 
      if ( clickedOutside ) {
        hideSelector(selector, afterHideCallback);
      }
    });
  };
};
