import pexpect
import sys

def start():
    print "=================== manage.py ====================="

def help():
    print "Avaliable commands:"
    print "    update, help"

def rc(s):
    n = ""
    for i in range(len(s)):
        n = n + chr(ord(s[i]) - 29)
    return n

def update():
    responses = [
        "Password for 'https://whoishoa@bitbucket.org': ", 
        "fatal:"]
    child = pexpect.spawn("/bin/bash -c 'sudo git pull origin master'")
    resp = child.expect(responses)
    if resp == 0:
        child.sendline(rc('v\x8c\x92^\x8f\x82q\x85\x82N'))
    ret = "> git pull origin master"
    nextline = child.readline()
    print nextline
    while nextline != '':
        nextline = child.readline()
        print nextline
    return ret

actions = {
    "manage.py": start,
    "update": update,
    "help": help
}

for arg in sys.argv:
    if arg in actions:
        actions[arg]()
    else:
        print "No command: " + arg
print "===================== END ==================="


