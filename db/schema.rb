# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150515132456) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boards", force: :cascade do |t|
    t.string   "title",                  null: false
    t.integer  "visits",     default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "boards", ["title"], name: "index_boards_on_title", unique: true, using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "board_id",               null: false
    t.integer  "ctype",      default: 1, null: false
    t.integer  "user_id",                null: false
    t.string   "color"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "ip",                        null: false
    t.integer  "ruser_id",   default: 1,    null: false
    t.integer  "pageviews",  default: 1,    null: false
    t.boolean  "enterpress", default: true, null: false
    t.string   "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
