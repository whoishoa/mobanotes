class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :ip, :null => false
      t.integer :ruser_id, :default => 1, :null => false
      t.integer :pageviews, :default => 1, :null => false
      t.boolean :enterpress, :default => true, :null => false
      t.string :color;
      
      t.timestamps
    end
  end
end
