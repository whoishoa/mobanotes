class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :board_id, :null => false
      t.integer :ctype, :default => 1, :null => false
      t.integer :user_id, :null => false
      t.string :color
      t.text :comment

      t.timestamps
    end
  end
end
