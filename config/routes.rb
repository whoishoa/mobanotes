Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  
  # Format: '*controller*#*method*'
  root 'index#home'
  
  get '/chat', :to => 'index#chat'
  get '/test/testlab', :to => 'index#testlab'
  get '/commands/oembed', :to => 'index#oembed'
 
  namespace :json do
    resources :comments
    resources :users
  end
  
  namespace :commands do
    get '/clearsession', :to => 'commands#clearsession'
    get '/encode', :to => 'commands#encode'
  end
  
  namespace :services do
    get '/websocket', :to => WebsocketRails::ConnectionManager.new
    post '/websocket', :to => WebsocketRails::ConnectionManager.new
  end
  
  get '/:title', :to => 'index#publicboard'
  
  # usernames must be three letters or more to avoid conflicts
  # usernames can be reclaimed to prevent pirates
  
  # / - public boards
  # /c/ - official company pages
  # /i/ - official interest pages (user runned or company runned)
  # /n/ - no edit, no delete public boards
  # /p/ - private boards (password required, information is deleted after everyone leaves)
  # /q/ - question boards
  # /d/ - (d for discover) content boards
  # /m/ - my private boards (invite/password options)
  # /my/ - my public boards. same as /whoishoa/
  # /t/thumai/ - private conversations with thumai. Same as /my/whoishoa/ if I was thumai
  # /ty/thumai/ - public conversations with thumai. Same as /thumai/whoishoa/ and /whoishoa/thumai/
  
  # /doc/ - documentation
  # /about/ - about us
  # /api/ - api
  # /test/ - internal testing
  # /commands/ - server commands
  # /services/ - server services
  # /json/ - ajax requests
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
