WebsocketRails::EventMap.describe do
  # You can use this file to map incoming events to controller actions.
  # One event can be mapped to any number of controller actions. The
  # actions will be executed in the order they were subscribed.
  #
  # Uncomment and edit the next line to handle the client connected event:
  #   subscribe :client_connected, :to => Controller, :with_method => :method_name
  #
  # Here is an example of mapping namespaced events:
  #   namespace :product do
  #     subscribe :new, :to => ProductController, :with_method => :new_product
  #   end
  # The above will handle an event triggered on the client like `product.new`.
  subscribe :client_connected, to: Boards::UserController, with_method: :client_connected
  subscribe :client_disconnect, to: Boards::UserController, with_method: :client_disconnected
  
  subscribe :change_color, to: Boards::UserController, with_method: :change_color
  subscribe :toggle_enter, to: Boards::UserController, with_method: :toggle_enter
    
  namespace :publicboard do
    subscribe :add_message, to: Boards::PublicController, with_method: :add_message
    subscribe :delete_message, to: Boards::PublicController, with_method: :delete_message
    subscribe :edit_message, to: Boards::PublicController, with_method: :edit_message
    subscribe :load_posts, to: Boards::PublicController, with_method: :load_posts
    subscribe :interval_posts, to: Boards::PublicController, with_method: :interval_posts
  end
  
end
